# React Bundle Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name reactbundle

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        0.6.0
Release:        %{revision}%{?dist}
Summary:        Icinga PHP library module for Icinga Web 2
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}

%if 0%{?fedora} || 0%{?rhel} || 0%{?amzn}
%if 0%{?rhel} == 7
%define php_scl         rh-php71
%endif # rhel 7
%if 0%{?rhel} == 6
%define php_scl         rh-php70
%endif # rhel 6
%endif # rhel os_family

%if 0%{?php_scl:1}
%define php_scl_prefix  %{php_scl}-
%endif

%define php             %{?php_scl_prefix}php

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       icingacli >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

%if 0%{?suse_version}
Requires:       %{php}-pcntl
Requires:       %{php}-posix
Requires:       %{php}-soap
Requires:       %{php}-sockets
%else # suse_version
# RedHat
Requires:       %{php}-cli
Requires:       %{php}-process
Requires:       %{php}-pcntl
Requires:       %{php}-soap
%endif # suse_version

%description
This module provides React as a third-party library for other Icinga Web
modules.

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

%clean
rm -rf %{buildroot}

%post
set -e

# Only for fresh installations
if [ $1 == 1 ]; then
    if [ ! -d /etc/icingaweb2/enabledModules ]; then
        mkdir /etc/icingaweb2/enabledModules
        chmod g=rwx,o= /etc/icingaweb2/enabledModules
    fi

    echo "Enabling icingaweb2 module '%{module_name}'"
    ln -svf /usr/share/icingaweb2/modules/%{module_name} /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%preun
set -e

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%files
%doc README.md
#LICENSE

%defattr(-,root,root)
%{basedir}

%changelog
* Tue May 21 2019 Markus Frosch <markus.frosch@icinga.com> - 0.6.0-1
- Update to 0.6.0

* Fri May 03 2019 Markus Frosch <markus.frosch@icinga.com> - 0.5.1-1
- Initial package version
